# Some cheatsheets

- https://brain.papey.fr/ops/tools/ansible.html

# Books

- [Ansible - From beginner to pro](https://www.apress.com/gp/book/9781484216606)
- [Mastering Ansible](https://www.packtpub.com/free-ebooks/reading-list/admin-and-networking/9781789951547)
- [Ansible - Gérez la configuration de vos serveurs et le déploiement de vos applications](https://www.editions-eni.fr/livre/ansible-gerez-la-configuration-de-vos-serveurs-et-le-deploiement-de-vos-applications-2e-edition-9782409025259)

# Playbooks examples

- https://stdio.space/ansible-galaxy

# Output

- [Callback plugin](https://docs.ansible.com/ansible/2.9/plugins/callback.html#managing-adhoc)
