install-packages:
	@xargs -d '\n' -- sudo apt -y install < .packages

check-user-present:
	@ansible all -m user -a 'name=ansible state=present'

# Easily create it
check-smonff-present:
	@ansible all -m user -a 'name=smonff state=present'

ping-one-host:
	@ansible lxc_debian --limit debian-01 -m ping

ping-host-02:
	@ansible lxc_debian --limit debian-02 -m ping

apt-install:
	@ansible all -m package -a 'name=ca-certificates state=present'

# Requires ca-certificates on Debian
wget:
	@ansible lxc_debian -m get_url -a 'url=https://files.balik.network/keybase.txt dest=/tmp'

list-modules:
	@ansible-doc --list

password-protected-user:
	@ansible-playbook --ask-vault-pass user.yml -vv

deploy-keys:
	@ansible-playbook --ask-vault-pass keys.yml -vv
